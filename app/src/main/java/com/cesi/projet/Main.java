package com.cesi.projet;


import com.cesi.projet.dataObjects.Oeuvre;
import com.cesi.projet.database.controller.AbstractController;
import com.cesi.projet.database.models.Category;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        AbstractController<Category> controller = new AbstractController<>();
        AbstractController<Oeuvre> controller2 = new AbstractController<>();

        Category category = controller.create(Category.class);
        category.setName("test");

        controller.update(category);

        List<Category> categories = controller.read();

        Oeuvre oeuvre = controller2.create(Oeuvre.class);
    }
}
