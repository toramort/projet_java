package com.cesi.projet.dataObjects;

import java.util.Date;

public class Album extends Oeuvre {

    private Liste morceaux;
    private Liste personalities;
    private int id_album;

    public Album(String title, Date date_oeuvre, boolean finished, Liste morceaux, Liste personalities) {
        super(title, date_oeuvre, finished);
        this.morceaux = morceaux;
        this.personalities = personalities;
    }

    @Override
    public String toString() {
        return "Album{" +
            "morceaux=" + morceaux +
            ", personalities=" + personalities +
            super.toString() +
            '}';
    }
}
